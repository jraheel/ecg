module FeatureSelection

using LinearAlgebra
using Statistics
using Revise
using Statistics
using OrthoMatchingPursuit
using Test
using ElasticArrays
using PyCall
using ShapML
using DataFrames
using Random
using ShapML
using RDatasets
using MLJ  # Machine learning
using StatsBase
using FeatureSelectors
using MultivariateStats
using ParallelKMeans

function select_largest_features(X, k::Int)
    """
        function select_largest_features(X, k::Int)
    
    selects k indices of columns of X with highest one norm

    Inputs: 2 inputs
        - X: the matrix of datapoints. 
        - k: the number of indicies (and therefore columns) required
    
    Outputs: 1 output
        - a vector of indices of size k
    """

    println("selecting the largest features")
    col_norms = norm.(eachcol(X), 1)
    inds = sortperm(-abs.(col_norms))
    return inds[1:k]
end


function select_features_via_univariate_test(X, y, k::Int, statistic::String)
    """
        function select_features_via_univariate_test(X, y, k::Int, statistic::String)
    
    selects top features which have the highest probability using a specified statistic

    Inputs: 4 inputs
        - X: the matrix of data. The rows of X are the datapoints, columns are features. 
        - y: the vector of labels. 
        - k: the number of features required
        - statistic: the statistical metric to be used, e.g. f test, χ^2, Pearson correlation
    
    Outputs: 1 output
        the indices of the features (columns) which in absolute value have the highest Pearson
        correlation with the output vector, the labels.
    """

    selector = UnivariateFeatureSelector(method=pearson_correlation, k=k)
    if statistic == "Pearson_correlation"
        println("selecting features via Pearson correlation")
        # do nothing, since by default the selector uses Pearson correlation
    elseif statistic == "f_test"
        println("selecting features via f test")
        selector = UnivariateFeatureSelector(method=f_test, k=k)
    elseif statistic == "Chi_squared"
        println("selecting features via Χ squared")
        selector = UnivariateFeatureSelector(method=chisq_test, k=k)
    end
    idx = select_features(
               selector,
               DataFrame(X, :auto),
               y
           )
    return map(x->Int(parse(Float64,x[2:end])),idx)
end


function select_features_via_omp(X, y, k::Int)
    """
        function select_features_via_omp(M, y, k::Int)
    
    selects few features using the Orthogonal Matching Pursuit algorithm. 

    Inputs: 2 inputs
        - X: the data matrix. X[i,:] is the i-th data point. M should be normalized so each
          column of X, norm(X[:,j]) = 1.0
        - y: the vector of labels. y[i] is the label for the i-th data point
    
    Outputs: 1 output
        a vector of k indices
    """

    println("Selecting features via OMP")
    normalized_X = copy(X)      # normalize X so that each column has unit norm
    tol = 1e-6                  # tolerance
    for i = 1:size(X,2)
        if abs(norm(X[:,i]) - 1.0) > tol # norm not equal to 1
            normalized_X[:,i] = normalized_X[:,i] ./ norm(normalized_X[:,i])
        end
    end
    idxls, cls, ϵls = lsomp(normalized_X, y; invert = true, verbose = true, ϵrel = eps())
    return idxls[1:k]
end


function select_features_via_Shapely_values(model, prediction_function, X, ninstances::Int, nsamples::Int, k::Int)
    """
        function select_features_via_Shapely_values(model, prediction_function, X, ninstances::Int, nsamples::Int, k::Int)
    
    selects a few features using the Shapely values, as per the algorithm described in the 
    paper by Strumbelj and Kononenko here https://link.springer.com/article/10.1007/s10115-013-0679-x.

    Inputs: 6 inputs
        - model: a trained machine learning model, like SVM, random forest etc.
        - prediction_function: a function that takes as input a trained model and test data in 
                               the form of a DataFrame as input, and returns a DataFrame 
                               of predictions
        - X: the matrix of test points
        - ninstances: the number of rows of X (number of data points) to be used in computing
                      the Shapely values. These rows are selected randomly from the full X
        - nsamples:   the number of Monte Carlo samples to be used in computing the Shapely values
    
    Outputs: 1 outputs
        - a vector of indices that represent the features (columns) of X with highest absolute
          average of Shapely effect
    """
    
    println("selecting features via Shapely values")
    explain = DataFrame(X, :auto)       # convert X to a DataFrame from a matrix
    explain = explain[shuffle(1:nrow(explain))[1:ninstances], :]    # select ninstances many rows from X at random


    reference = DataFrame(X, :auto)     # opetion reference population to compute the baseline prediction

    sample_size = nsamples              # the number of Monte Carlo samples to be used

    data_shap = ShapML.shap(explain = explain,  # computing the Shapely values
                            reference = reference,
                            model = model,
                            predict_function = prediction_function,
                            sample_size = sample_size,
                            seed = 1
                            )

    shap_effect = data_shap.shap_effect # extract the Shapely effect column per feature 
    features = unique(data_shap.feature_name)   # total number of features (should be equal to number of columns of X)
    inc = Int(size(data_shap,1) / length(features)) # number of Shapely values per feature
    D = Dict()
    for i = 1:length(features)
        st_ind = inc*(i-1) + 1
        end_ind = inc*i
        merge!(D,Dict(features[i]=>mean(abs.(shap_effect[st_ind:end_ind]))))    # mean of Shapely effect per feature
    end
    
    sorted_Shapely_vals = collect(keys(sort(D; byvalue=true)))
    features = map(x->Int(parse(Float64,x[2:end])),sorted_Shapely_vals)
    return features[end-k:end]  # the features with the highest mean Shapely effect occur at the bottom due to sorting in asceding order
end


function cluster_features_per_class_via_kmeans(X, features_selected)
    """
        function cluster_features_per_class_via_kmeans(X, features_selected)
    
    computes clusters of features for a data matrix having two classes only. This is useful when we want to see
    which features are clustered together for the sake of reducing dimensions.

    Inputs: 2 inputs
        - X: the data matrix. X[i,:] is the i-th datapoint
        - features_selected: an array of integers, where each entry is a feature index
    
    Outputs: 1 output
        - feature_clusters: a diciontary whose keys are the features_selected, and values are arrays of integers. 
                            E.g. if feature_clusters[7] = [17, 27, 7, 5, 19], this means the features (i.e. columns of X)
                            that belong to the cluster features number 7 belongs to are 17, 27, 7, 5, and 19. Of course
                            feature number 7 must belong to the cluster too.
    """
    clusters = kmeans(Yinyang(), X, length(features_selected))
    feature_clusters = Dict()
    for feature in features_selected
        feature_clusters[feature] = findall(x->x==clusters.assignments[feature], clusters.assignments)
    end
    return feature_clusters
end


function cluster_features_per_class_via_correlation(X, features_selected)
    """
        function cluster_features_per_class_via_correlation(X, features_selected)
    
    computes correlation of all the features with each feature in an array passed as a parameter, to identify features
    which are the most correlated with a given feature. This allows us to cluster features which are most similar in
    some sense to a given feature. 

    Inputs: 2 inputs
        - X: the data matrix. X[i,:] is the i-th datapoint
        - features_selected: an array of integers, where each entry is a feature index
    
    Outputs: 1 output
        - feature_clusters: a dictionary whose keys are the features_selected. The values are a list of indices of
                            columns of X (i.e. features) which is sorted in descending order of correlation with the 
                            key. E.g. feature_clusters[i] = [i, 1, 5, 7, 11, ..] means the features with the highest 
                            absolute correlation with the feature i are i, 1, 5, 7, 11 and so on.
    """
    feature_clusters = Dict()
    for feature in features_selected
        correlations = zeros(size(X,2))
        for i = 1:size(X,2)
            correlations[i] = cor(X[:,feature], X[:,i])
        end
        feature_clusters[feature] = sortperm(-abs.(correlations))
    end
    return feature_clusters
end


end # End of module FeatureSelection


