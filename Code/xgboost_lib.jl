module xgboost_lib

using XGBoost
using Statistics
using StatsBase
using MultivariateStats
using LinearAlgebra

function _train_xgboost(X, y, num_round, num_classes, eta=1.0, 
                            max_depth=6, objective="multi:softmax")
    """
        function _train_xgboost(X, y, num_round, num_classes, _eta, _max_depth, objective)
    
    wrapper on top of XGBoost library's training function. Trains and returns an instance 
    of XGBoost

    Inputs: 7 inputs
        - X: the matrix of training points. X[i,:] is the i-th data point
        - y: the vector of labels. y[i] is the label of the i-th data point
        - num_round: the number of rounds for XGBoost
        - num_classes: the number of classes for multi-class classification
        - eta: the eta parameter for XGBoost
        - max_depth: the maximum depth for XGBoost
        - objective: the metric to optimize. Available options are "binary:logistic", 
                     "multi:softmax", and "multi:softprob". When using "multi:softprob", 
                     the predictions would be a vector of number of datapoints, length(y) 
                     times number of classes. 

    Outputs: 1 output
        - the trained XGBoost instance with the given parameters on X and y
    """
    return xgboost(X, num_round, label=y, eta=eta, 
                max_depth=max_depth, objective=objective,num_class = num_classes)
end


function _make_predictions(X, bst)
    """
        function _make_predictions(X, bst)
    
    uses the prediction function from XGBoost library to make predictions given a matrix of points
    
    Inputs: 2 inputs
        - X: the matrix of training points. X[i,:] is the i-th data point
        - bst: an instance of trained XGBoost
    
    Outputs: 1 output
        - a vector of predicted labels
    """
    return XGBoost.predict(bst, X)
end


function _compute_accuracy(X, y, bst)
    """
        function _compute_accuracy(X, y, bst)
    
    helper routine to compute the accuracy of predictions made using a trained XGBoost model

    Inputs: 3 inputs
        - X: the matrix of datapoints. X[i,:] is the i-th datapoint
        - y: the vector of true labels for the datapoints. y[i] is the label for i-th datapoint
        - bst: trained XGBoost instance
    
    Outputs: 1 output
        - the accuracy of predictions made on X using bst, measured in terms of how many of
          the predicted labels matched the actual labels
    """
    yp = _make_predictions(X, bst)
    return sum(yp .== y) / length(yp) * 100
end

end # end of xgboost_lib module