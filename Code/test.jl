
using ShapML
using LinearAlgebra
using DataFrames
using RDatasets
using MLJ  # Machine learning
using Gadfly  #
using ScikitLearn
using LIBSVM
using PyCall
using MLJScikitLearnInterface
using HypothesisTests
using Random, Distributions, StatsBase
using FeatureSelectors
using MultivariateStats
using DataStructures
using XGBoost

include("DataRW.jl")
include("Coresets.jl")
include("xgboost_lib.jl")
include("svm.jl")
include("FeatureSelection.jl")
include("PerformanceMetrics.jl")

cd("/Volumes/Garbage/Purdue/Courses/CS699/With Drineas/ECG/")

function read_data(fname, field)
    M = DataRW.read_mat_file(fname, field)
    return DataRW._remove_rows_with_nans(M)
end

function _get_downsampled_dataset(dset, n, sampling_type)
    if sampling_type == "random"    # random coreset
        return Coresets.random_sample(dset, n)
    else                            # coreset computed via clustering
        return Coresets.sample_via_clustering(dset, n)
    end
end

function run_full_dataset(dsets_list, field, normalization_type, balance, sampling_type, p, feature_selection_type,
                         nfeatures, alg, nrounds)
    @show normalization_type, balance, feature_selection_type, nfeatures, p, alg, nrounds
    # 1. read datasets
    dsets = Dict()
    println("reading data...")
    for dset in dsets_list
        dset_name = split(split(dset, "/")[end],"_")[4]
        dsets[dset_name] = read_data(dset, field)
    end
    
    dset_points = Int.(zeros(length(dsets_list)))
    ncols = 0
    for i = 1:length(dsets_list)
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        dset_points[i], ncols = size(dsets[dset_name])
    end

    X = []
    y = zeros(1)
    for i = 1:length(dsets_list)
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        if i == 1
            X = dsets[dset_name]
            y = zeros(size(X,1))
        else
            dset = dsets[dset_name]
            X = [X;dset]
            y = [y;(i-1)*ones(size(dset,1))][:]
        end
    end

     # 2. normalize
    if normalization_type == "z_scores"
        println("normalizing data...")
        X = DataRW._z_score_transform(X)
    end


    start_range = 1
    end_range = Int(dset_points[1])
    for i = 1:length(dsets_list)-1
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        dsets[dset_name] = X[start_range:end_range,:]
        start_range = end_range + 1
        end_range = end_range + dset_points[i+1]
    end
    
    
    # 3. balance the data so that all classes have roughly equal number of members
    X_rejected = []
    y_rejected = []
    if balance
        n = minimum(dset_points) 
        X = []
        y = zeros(1)
        for i = 1:length(dsets_list)
            dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
            X_sel, X_rej = _get_downsampled_dataset(dsets[dset_name], Int(n), sampling_type)
            y_sel = (i-1)*ones(size(X_sel,1))
            y_rej = (i-1)*ones(size(X_rej,1))
            if i == 1
                X = X_sel
                y = y_sel

                X_rejected = X_rej
                y_rejected = y_rej
            
            else
                X = [X; X_sel]
                y = [y; y_sel]

                X_rejected = [X_rejected; X_rej]
                y_rejected = [y_rejected; y_rej]
            end
        end
    end
    return X, y, X_rejected, y_rejected
end

parent_folder = pwd()
data_folder = "/Data/"
datasets = Dict("afib" => "challenge_training2017_cases_afib_ecg_corr_metrics.mat",
                "noisy" => "challenge_training2017_cases_noisy_ecg_corr_metrics.mat",
                "normal" => "challenge_training2017_cases_normal_ecg_corr_metrics.mat",
                "other" => "challenge_training2017_cases_other_ecg_corr_metrics.mat")

field = "all_corr_metrics"

# afib = read_data(parent_folder*data_folder*datasets["afib"], field)
# noisy = read_data(parent_folder*data_folder*datasets["noisy"], field)
# normal = read_data(parent_folder*data_folder*datasets["normal"], field)
# other = read_data(parent_folder*data_folder*datasets["other"], field)

afib = parent_folder*data_folder*datasets["afib"]
normal = parent_folder*data_folder*datasets["normal"]
noisy = parent_folder*data_folder*datasets["noisy"]
other = parent_folder*data_folder*datasets["other"]

X_afib = read_data(afib, field)
X_normal = read_data(normal, field)
X_noisy = read_data(noisy, field)
X_other = read_data(other, field)

normalization_types = ["none", "z_scores"]
balance_types = [false true]
sampling_type = "random"
feature_selection_types = ["largest_features", "OMP", "Pearson"]
p = 90.0
nreps = 10
nrounds = 5
_eta = 1.0
_max_depth = 6
num_features = [Vector(1:10); 48]
algs = ["svm"]#["xgboost", "svm"]

dsets = [afib normal noisy other]

nparams = 6
for alg in algs
    for nfeatures in num_features
        for sampling_type in feature_selection_types
            for normalization in normalization_types
                for balance in balance_types            
                    for i = 1:nreps
                        println("rep #: ", i)
                        run_full_dataset(dsets, field, normalization, balance, 
                                                            sampling_type, p, sampling_type, 
                                                            nfeatures, alg, nrounds)
                    end
                end
            end
        end
    end  
end

