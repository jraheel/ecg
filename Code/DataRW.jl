module DataRW

using DelimitedFiles
using Statistics
using LinearAlgebra, SparseArrays, Random, Distributions
using MatrixDepot
using MAT
using BEDFiles
using DataFrames
using CSV
using StatsBase


function read_mat_file(fname::String, field::String)
    """
        function read_mat_file(fname, field)

    reads a file stored in .mat format and returns the data in it

    Inputs: 2 inputs
        - fname: the full path to the file to be read. Type: String
        - field: the name of the field which has the data in the dictionary when a .mat file is read
    
    Outputs: 1 output
        the data in the .mat file pointed to by field
    """
    file = matopen(fname)
    D = read(file)
    close(file)
    return D[field]
end

function readlibsvm(fname::String, shape)
    """
        readlibsvm(fname::String, shape)

    reads a file in LIBSVM format. It is adapted from https://github.com/dmlc/XGBoost.jl/blob/master/demo/basic_walkthrough.jl

    Inputs: 2 inputs
        - fname: the full path to the file to be read. Type: String
        - shape: a tuple of two integers, corresponding to the shape of the matrix written in the file. Type: tuple

    Ouptuts: 1 output
        - the data as a matrix. The first column is the bias column of all ones, the last column is the vector of
            labels. The datapoints themselves appear between columns 2 to second last columns inclusive.

    # Example:
    readlibsvm('test.txt', (1000, 20000))
    """
    dmx = zeros(Float64, shape)
    label = Float64[]
    fi = open(fname, "r")
    cnt = 1
    for line in eachline(fi)
        line = split(line, " ")
        push!(label, parse(Float64, line[1]))
        line = line[2:end]
        for itm in line
            itm = split(itm, ":")
            dmx[cnt, parse(Int, itm[1])] = parse(Float64, itm[2])
        end
        cnt += 1
    end
    close(fi)
    return [ones(shape[1]) dmx label]
end


function libsvm2sparsemat(fname::String, n_cols::Int)
    """
        libsvm2sparsemat(fname::String, n_cols)
    
    reads a file in LIBSVM format into a sparse matrix. It attaches a column of ones to the start
    of the matrix. The labels is appears in the last column of the assembled matrix.

    Inputs: 2 inputs
        - fname: the full path to the file to be read. Type: String
        - n_cols: the number of columns of the matrix in the file. Type: Int
    
    Outputs: 1 output
        - the data in the file assembled into a sparse matrix. Type: Sparse Array
    """
    row_inds = Int[]
    col_inds = Int[]
    values = Float64[]
    fi = open(fname, "r")
    row = 1
    for line in eachline(fi)
        # appending a one as the first element of every data point
        push!(row_inds, row)
        push!(col_inds, 1)
        push!(values, 1.0)

        line = split(line, " ")

        # the first element in the LIBSVM format in each line is the label
        push!(row_inds, row)
        push!(col_inds, n_cols + 2)
        push!(values, parse(Float64, line[1]))

        # populate the data as row -> row_inds, col -> col_inds, value -> values
        line = line[2:end]
        for itm in line
            itm = split(itm, ":")
            push!(row_inds, row)
            push!(col_inds, parse(Int, itm[1]) + 1)
            push!(values, parse(Float64, itm[2]))
        end
        row += 1
    end
    close(fi)
    return sparse(row_inds, col_inds, values)
end


function read_HGDP_mat_files(fname::String, pos_classes = ["EUROPE"])
    """
        function read_HGDP_mat_files(fname::String)
    reads the HGDP dataset files stored in MATLAB matrix (.mat file) format.
    The HDGP files are stored with five keys: "DGDPpeopleid", "rsinfo", "A", "populations", and "regions". A is the
    key that holds the matrix of all data points. regions are different geographical regions like EUROPE, AFRICA, etc.
    The function converts this dataset into binary classes, using one-versus-all approach, where pos_class is
    one class and all the rest are the other class. The function assings a label +1 to all datapoints whose label is
    pos_class, and -1 to all other data points.

    Inputs: 2 inputs
        - fname: the name of the mat file to beread, e.g. "./HGDP1.mat"
        - pos_class: the region which shall be used as the "positive class", i.e. data points with this label shall
                     be assigned a label of +1, and all others shall be labeled -1.

    Outputs: 1 output
        - Matrix of data points and binary labels. Last column of the matrix contains the labels.
    """
    file = matopen(fname)
    D = read(file)
    close(file)
    M = convert(Matrix{Float64}, D["A"])      # the data points
    n = size(M,1)
    y = ones(n)
    labels = D["regions"]
    for i = 1:n
        if !(labels[i] in pos_classes)
            y[i] = -1.0
        end
    end
    return [ones(n) M y]
end

function read_STL10_mat(fname::String, pos_classes=[])
    """
        function read_STL10_mat(fname::String, pos_classes=[])
    reads the STL-10 dataset available here https://cs.stanford.edu/~acoates/stl10/ stored as .mat
    (MATLAB matrix) files. The datafile has fields titled X, y, class_names, fold_indices. X is
    the matrix of datapoints, y is the vector of labels, class_names are the actual titles of the
    classes. There are ten classes { "airplane", "bird", "car", "cat", "deer", "dog", "horse",
    "monkey", "ship", "truck"}. Accordingly y contains integers from 1 to 10. 
    This function reads the file, extracts X and y. Relabels y to contain only 1.0 and -1.0 to 
    signify conversion to just two classes.

    Inputs: 2 inputs
        - fname: the name of the mat file to beread, e.g. "./HGDP1.mat"
        - pos_class: the classes which shall be used as the "positive class", i.e. data points with this label shall
                     be assigned a label of +1, and all others shall be labeled -1.

    Outputs: 1 output
        - Matrix of data points and binary labels. Last column of the matrix contains the labels.

    """
    file = matopen(fname)
    D = read(file)
    close(file)
    X = convert(Matrix{Float64}, D["X"])
    y = D["y"]
    for i = 1:length(y)
        if y[i] in pos_classes
            y[i] = 1.0
        else
            y[i] = -1.0
        end
    end
    return [ones(length(y)) X y]
end


function read_dlm(fname::String)
    """
        read_dlm(fname::String, dtype)

    uses the readdlm function from DelimitedFiles library to read the contents of stored in a file on disk
    and returns as a matrix.

    Inputs: 1 input
        - fname: the path to the file to be read
    
    Outputs: 1 output
        a matrix containing all the data in the file passed as the fname parameter. This shall be a
        matrix whose each entry shall be a Float64
    """
    return readdlm(fname, Float64)
end


function read_sep(fname::String, pos_classes = ["BRCA"])
    """
        function read_sep(fname::String, pos_classes = ["BRCA"])
    reads data stored in a csv file specified into a matrix. There should be a file titled 
    "labels.txt" in the same folder as the csv file to read labels from and numerically 
    assign 1s and -1s accordingly. The last column of the assembled matrix has the numeric 
    labels. +1 signifies the label is in the pos_classes list , -1 specifies otherwise. 

    Inputs: 2 inputs
        - fname      : name of the csv file to read data from
        - pos_classes: a list of all labels that form one single class
    
    Outputs: 1 output
        - a matrix of Flaot64s. The first columns is appended with all ones, as bias column. 
          The last column has the labels, with each entry being either +1 or -1.
    """
    df = CSV.read(fname, DataFrame)
    M = Matrix(df[1:end, 2:end])
    label_file = join(split(fname, "/")[1:end-1], "/") * "/labels.txt"
    ifile = open(label_file)
    labels = readlines(ifile)
    close(ifile)
    n = size(M,1)
    @assert length(labels) == n
    y = ones(n)
    for i = 1:n
        if !(labels[i] in pos_classes)
            y[i] = -1.0
        end
    end
    return [ones(size(M,1),1) M y]
end


function read_plink(fname::String, pos_classes = ["EUROPE"])
    """
        function read_plink(fname)

    reads a plink genome file and assembled a matrix of the data contained in it.
    This function makes use of Python library plinkio to read the labels.
    
    Inputs: 1 input
        - fname: name of the .bed file containing the data. Note the same folder containing
                 the .bed file must also have the .fam and .bim files, e.g. 
                 if fname = "data/test.bed", then the folder data must also contain test.fam, and
                 test.bim files.
    
    Outputs: 1 output
        - a matrix of Float64s that has the first column as all ones, and last column as the 
          labels. The data in between are the samples contained in the .bed file.
    """
    # read the datapoints 
    M = BEDFile(BEDFiles.datadir(fname))
    M = convert(Matrix{Float64}, M)
    M[M .== 3.0] .= -1.0
    M[M .== 2.0] .= 3.0
    M[M .== 1.0] .= 2.0
    M[M .== 0.0] .= 1.0
    M[M .== -1.0] .= 0.0
    M[isnan.(M)] .= 0.0
    
    # read the labels
    label_file = join(split(fname, "/")[1:end-1], "/") * "/labels.txt"
    ifile = open(label_file)
    labels = readlines(ifile)
    close(ifile)
    n = size(M,1)
    @assert length(labels) == n
    y = ones(n)
    for i = 1:n
        if !(labels[i] in pos_classes)
            y[i] = -1.0
        end
    end
    return [ones(size(M,1),1) M y]
end


function _remove_rows_with_nans(M)
    """
        function _remove_rows_with_nans(M)
    removes all rows in a given matrix containing NaNs

    Inputs: 1 input
        - M: a matrix of Float64 of size m-by-n
    
    Outputs: 1 output
        - submatrix of M k-by-n, where k <= m. This submatrix does not contain any rows of
          M that have NaNs in them 
    """
    nanrows = any(isnan.(M), dims=2)    
    return M[.!vec(nanrows), :]
end


function _mean_center_data(M)
    """
        function _mean_center_data(M)
    centers the data contained in a matrix by subtracting from each entry of a column of M, the 
    mean of that column. E.g. M[i,j] -> M[i,j] - mean(M[:,j]).

    Inputs: 1 input
        - M: the data in a matrix of Float64
    
    Outputs: 1 output
        - a matrix the same size as M with each column mean centered
    """
    return M .- mean(M, dims=1)
end


function _z_center_data(M)
    """
        function _z_center_data(M)
    converts each entry of M to a z-score, i.e. standard normal distribution probability value.
    M[i,j] = (M[i,j] - mean(M[:,j])) / std(M[:,j]). Hence all columns after z-centering shall
    have mean 0.0 and std 1.0.

    Inputs: 1 input
        - M: the matrix of data, of type Float64.
    
    Outputs: 1 output
        - a matrix the same size as M, with each column z-centered
    """
    M .-= mean(M, dims=1)
    return M ./ std(M, dims=1)
end

function _z_score_transform(M)
    """
        function _z_score_transform(M)
    standardizes a matrix column-wise so that each column is centered (mean 0.0), 
    scaled (variance = 1.0), and the values are z scores, i.e. signed number of deviations
    by which the value is above the mean.

    Inputs: 1 input
        - M: the matrix of data

    Outputs: 1 output
        -  the standardized matrix
    """
    dt = fit(ZScoreTransform, M; dims=1, center=true, scale=true)
    return StatsBase.transform(dt, M)
end


function _unit_range_normalization(M)
    """
        function _unit_range_normalization(M)
    Transforms the dataset to scale features so that they lie in the interval [0,1]
    
    Inputs: 1 input
        - M: the matrix of data

    Outputs: 1 output
        -  the standardized matrix
    """
    dt = fit(UnitRangeTransform, M, dims=1)
    return StatsBase.transform(dt,M)
end


function _robust_transform(M)
    """
        function _robust_transform(M)

    Transforms the dataset to scale columns using the robust transform, so 
    x[i] => (x[i] - Q_1(x)) / (Q_3(x) - Q_1(x)), where x is a vector
    
    Inputs: 1 input
        - M: the matrix of data

    Outputs: 1 output
        -  the standardized matrix
    """
    for i = 1:size(M,2)
        v = M[:,i]
        transormed_v = (v .- percentile(v,25)) ./ (percentile(v,75) - percentile(v,25))
        M[:,i] = transormed_v
    end
    return M
end


function shuffle_dataset(X, y)
    """
        function shuffle_dataset(X, y)
    
    Shuffles the given dataset randomly

    Inputs: 2 inputs
        - X: the matrix of datapoints. X[i, :] is the i-th datapoint
        - y: the vector of labels. y[i] is the label for the i-th datapoint
    
    Outputs: 2 outputs
        - dset  : the shuffled datapoints
        - labels: the labels for the shuffled datapoints
    """
    dset = [X y]
    dset = dset[shuffle(1:end), :]                  # shuffle the training dataset
    labels = dset[1:end, size(dset, 2):end][:]      # get the labels
    dset = dset[1:end, 1:size(dset,2) - 1]          # discard the last column, containing labels
    return dset, labels
end


function stratified_sample(n::Int64, y::Vector{Float64})
    """
        function stratified_sample(n::Int64, y::Vector{Float64})
    
    samples n indices each for each of the unique elements contained in y
    E.g. if y has 0.0, 1.0, and 2.0, three elements with multiple copies of each, and n = 2, then
    the function with sample 2 indicies each for each of 0.0, 1.0, and 2.0.

    Inputs: 2 inputs
        - y: a vector of Float64 with multiple copies of some elements
        - n: number of indicies required per unique element in y
    
    Output: 1 output
        - a vector of indices of length n*length(unique(y)). If inds is the vector returned
          by the function, with inputs y and n, then y[inds] will have n copies of each of 
          the unique elements in y
    """
    classes = unique(y)             # unique labels
    all_indices = 1:length(y)
    inds = []
    for i = 1:length(classes)
        inds_chosen = sample(all_indices[y.==classes[i]], n, replace = false)
        inds = [inds;inds_chosen]
    end
    return inds
end


function split_data(M, p::Float64)
    """
        function split_data(M, p::Float64)
    
    splits a dataset into train and test sets, in the proportion p% train and (100 - p)% test.
    It also ensures that the test set is balanced in the sense that each class has equal number
    of datapoints.

    Inputs: 2 inputs
        - M: the data matrix. M[i,:] is the i-th datapoint. The last column of M are the labels
        - p: the percentage of M to be assigned to the training set

    Output: 2 outputs
        - the train and test sets, both matrices.
    """
    y = M[:,end:end][:]                 # the labels of the points
    nclasses = length(unique(y))        # the number of classes
    els_per_class_in_dataset = collect(values(countmap(y))) # number of elements for each class
    elements_per_class = Int(trunc((size(M,1) - Int(trunc(p*size(M,1)/100)))/nclasses))
    min_elements = minimum(els_per_class_in_dataset)
    # making sure there are elements of each class in both test and train set, and test set is balanced 
    n = min(Int(trunc(min_elements/2)),elements_per_class)
    test_inds = stratified_sample(n, y)
    train_inds = collect(setdiff(Set(1:length(y)), Set(test_inds)))
    return M[train_inds,:], M[test_inds,:]
end


function _remove_zero_cols(M)
    """
        function _remove_zero_cols(M)
    
    removes any columns which consists entierly of zeros

    Inputs: 1 input
        - M: a matrix of Float64
    
    Outputs: 1 output
        a submatrix of M having the same number of rows as M, but possibly lesser number of
        columns. All columns which are composed entirely of zeros in M are removed. 
    """
    zero_col_inds = zeros(Int, 0)
    for i = 1:size(M,2) 
        if unique(M[:,i]) == zeros(1)   
            push!(zero_col_inds, i)     # take note of the index of a zero column 
        end
    end
    if length(zero_col_inds) > 0        # if zero columns detected
        return M[1:end, 1:end .!= zero_col_inds]
    else                                # no zero columns found
        return M
    end
end



function write_to_file(dset, folder, results, header)
    """
        write_to_file(dset, folder, results)

    writes results to a file of running regression on a dataset to a text file.

    Inputs: 3 inputs
        - dset   : the name of the dataset for whom the results are to be written
        - folder : the output folder in which the results should be written to a text file
        - results: a matrix having all the results. This should have four columns, which in order should
                   contain the numbers for coreset_size, optimization_time, speedup, accuracy.
        - header : the name of the columns separated by commas. This shall be written 
    
    Outputs: 0 output
        - does not output anything. Instead creates a text file on disk and writes the results to it.
    """

    fname = pwd() * "/" * folder * "/" * dset * ".txt"
    touch(fname)
    ofile = open(fname, "w")
    write(ofile, header * "\n")
    writedlm(ofile,results, ",")
    close(ofile)
end


function write_dataset(dset, fname)
    """
        function write_dataset(dset, fname)
    writes a matrix to file in comma separated format

    Inputs: 2 inputs
        - dset: the matrix containing the data
        - fname: the full path and name of file data needs to be written to

    Outputs: 0 output
        does not return anything, just writes data to file
    """
    touch(fname)
    ofile = open(fname, "w")
    writedlm(ofile,dset, ",")
    close(ofile)
end

end # end of DataRW module