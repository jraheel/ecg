using MAT
using CSV, DataFrames, MLJ
using Statistics
using StatsBase
using MultivariateStats
using LinearAlgebra
using ParallelKMeans
using XGBoost
using LIBSVM
using PyCall
using ParallelKMeans

include("DataRW.jl")
include("Coresets.jl")
include("xgboost_lib.jl")
include("svm.jl")
include("FeatureSelection.jl")
include("PerformanceMetrics.jl")

cd("/Volumes/Garbage/Purdue/Courses/CS699/With Drineas/ECG/")

function read_data(fname, field)
    M = DataRW.read_mat_file(fname, field)
    return DataRW._remove_rows_with_nans(M)
end



function extract_largest_feature(ofolder)
    for i = 1:length(datasets)
        dset1 = read_data(parent_folder*data_folder*datasets[i])
        dset1 = DataRW._remove_rows_with_nans(dset1)
        for j = i+1:length(datasets)
            @show split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4]
            dset2 = read_data(parent_folder*data_folder*datasets[j])
            dset2 = DataRW._remove_rows_with_nans(dset2)
            labels = [ones(size(dset1,1));-ones(size(dset2,1))]
            M = [dset1;dset2]
            col_means = mean(M,dims=1)
            max_ind = argmax(col_means)[2]
            X = [M[:,max_ind] labels]
            fname = split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4] * "_" * string(size(X,2)-1) * ".txt"
            ofile = ofolder * fname
            DataRW.write_dataset(X,ofile)
        end
    end
end


function gen_pca_projection(dset1, dset2, outdimens, normalization_type, without_largest_feature)
        labels = [ones(size(dset1,1));-ones(size(dset2,1))]
        stacked_M = [dset1;dset2]
        @show size(stacked_M)
        if without_largest_feature 
            col_means = mean(stacked_M, dims=1)
            max_ind = argmax(col_means)[2]
            stacked_M = stacked_M[1:end,1:end .!= max_ind]
        end
        @show size(stacked_M)
        X = ones(1,1)
        if normalization_type == "mean_centering"
            println("mean centering")
            X = DataRW._mean_center_data(stacked_M)'
        elseif normalization_type == "z_centering"
            println("z centering")
            X = DataRW._z_center_data(stacked_M)'
        elseif normalization_type == "z_scores"
            println("z scores")
            X = DataRW._z_score_transform(stacked_M)'
        elseif normalization_type == "unit_range"
            println("unit range")
            X = DataRW._unit_range_normalization(stacked_M)'
        else
            println("unnormalized")
            X = stacked_M'
        end
        M = fit(PCA, X; pratio = 1.0, maxoutdim = outdimens)
        Y = MultivariateStats.transform(M,X)'
        return [Y labels]
end


function get_pca_vars(dset1, dset2, normalization_type, without_largest_feature)
    # labels = [ones(size(dset1,1));-ones(size(dset2,1))]
    stacked_M = [dset1;dset2]
    @show size(stacked_M)
    if without_largest_feature 
        col_means = mean(stacked_M, dims=1)
        max_ind = argmax(col_means)[2]
        stacked_M = stacked_M[1:end,1:end .!= max_ind]
    end
    @show size(stacked_M)
    X = ones(1,1)
    if normalization_type == "mean_centering"
        println("mean centering")
        X = DataRW._mean_center_data(stacked_M)'
    elseif normalization_type == "z_centering"
        println("z centering")
        X = DataRW._z_center_data(stacked_M)'
    elseif normalization_type == "z_scores"
        println("z scores")
        X = DataRW._z_score_transform(stacked_M)'
    elseif normalization_type == "unit_range"
        println("unit range")
        X = DataRW._unit_range_normalization(stacked_M)'
    else
        println("unnormalized")
        X = stacked_M'
    end
    M = fit(PCA, X; pratio = 1.0)
    return [principalvars(M); tprincipalvar(M); tresidualvar(M)]
end


function run(outdimens, normalization_type, ofolder, without_largest_feature)
    for i = 1:length(datasets)
        dset1 = read_data(parent_folder*data_folder*datasets[i])
        dset1 = DataRW._remove_rows_with_nans(dset1)
        for j = i+1:length(datasets)
            @show split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4]
            dset2 = read_data(parent_folder*data_folder*datasets[j])
            dset2 = DataRW._remove_rows_with_nans(dset2)
            projection = gen_pca_projection(dset1,dset2,outdimens,normalization_type,without_largest_feature)        
            fname = split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4] * "_" * string(size(projection,2)-1) * ".txt"
            ofile = ofolder * fname
            DataRW.write_dataset(projection,ofile)
        end
    end
end


function run_principal_vars(normalization_type, ofolder, without_largest_feature)
    for i = 1:length(datasets)
        dset1 = read_data(parent_folder*data_folder*datasets[i])
        dset1 = DataRW._remove_rows_with_nans(dset1)
        for j = i+1:length(datasets)
            @show split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4]
            dset2 = read_data(parent_folder*data_folder*datasets[j])
            dset2 = DataRW._remove_rows_with_nans(dset2)
            vars = get_pca_vars(dset1, dset2, normalization_type, without_largest_feature)
            # projection = gen_pca_projection(dset1,dset2,outdimens,normalization_type,without_largest_feature)        
            fname = split(datasets[i],"_")[4] * "_" * split(datasets[j],"_")[4] * ".txt"
            ofile = ofolder * fname
            DataRW.write_dataset(vars,ofile)
        end
    end
end


function _get_downsampled_dataset(dset, n, sampling_type)
    if sampling_type == "random"    # random coreset
        return Coresets.random_sample(dset, n)
    else                            # coreset computed via clustering
        return Coresets.sample_via_clustering(dset, n)
    end
end

function _get_features_indices(selection_type, M, y, k)
    if selection_type == "Pearson" # Pearson Correlation
        return FeatureSelection.select_features_via_univariate_test(M, y, k, "Pearson_correlation")
    elseif selection_type == "largest_features"
        return FeatureSelection.select_largest_features(M, k)
    elseif selection_type == "OMP"
        return FeatureSelection.select_features_via_omp(M, y, k)
    end
end



function get_accuracy(ds1, ds2, field, normalization_type, balance, sampling_type, feature_selection_type,
                         p, nfeatures, alg, kernel_type, nrounds, _eta, _max_depth)
    # 1. read data, remove rows containing NaN
    dset1 = read_data(ds1, field)
    dset2 = read_data(ds2, field)
    println("done reading data from disk")

    # 2. if we need to balance the data so that both datasets have roughly equal number of members
    if balance
        n = min(size(dset1,1), size(dset2,1))
        if size(dset1,1) == n   # dset1 is the smaller of the two
           dset2 = _get_downsampled_dataset(dset2, n, sampling_type)
        else                    # dset2 is smaller dataset
            dset1 = _get_downsampled_dataset(dset1, n, sampling_type)
        end
        println("datasets balanced")
        @show size(dset1), size(dset2)
    end

    M = [dset1;dset2]
    @show size(M)
    println("removing zero columns")   
    M = DataRW._remove_zero_cols(M)
    @show size(M)

     # 3. if normalization is required
    if normalization_type == "z_scores"
        M = DataRW._z_score_transform(M)
        println("normalized data")
    end
       

    # 4. generate labels
    y = [ones(size(dset1,1));zeros(size(dset2,1))] # lables +1 for all dset1 elements, -1 for dset2
    
    # 5. split into train and test sets
    train_X, test_X = DataRW.split_data([M y], p)

    train_y = train_X[1:end,end:end][:]     # training labels
    train_X = train_X[1:end,1:end-1]        # the training data points

    test_y = test_X[1:end, end:end][:]      # testing labels
    test_X = test_X[1:end, 1:end-1]         # the testing data points
    println("splitting into training and testing sets done")

    # 6. select features
    if nfeatures <= size(train_X,2) # need to sample features
        idx = _get_features_indices(feature_selection_type, train_X, train_y, nfeatures)
        train_X = train_X[:,idx]
        test_X = test_X[:,idx] 
    end
    @show size(train_X), size(test_X), countmap(test_y)[1], countmap(test_y)[0.0]
    
    # 7. train the model
    println("beginning training, and testing the trained model")
    train_X, train_y = DataRW.shuffle_dataset(train_X, train_y)
    if alg == "svm"
        println("\nRunning SVM\n")
        model, t, w, b = svm.train_model(train_X, train_y, kernel_type)
        return svm.compute_accuracy(model, test_X, test_y), size(train_X,2)
    elseif alg == "xgboost"    
        println("\nRunning XGBoost\n")
        bst = xgboost_lib._train_xgboost(train_X, train_y, nrounds, _eta, _max_depth)
        return xgboost_lib._compute_accuracy(test_X, test_y, bst), size(train_X,2)
    end
end


function run_ml(dset1, dset2, normalization, balance, sampling_type, feature_selection_type, p, nreps, nrounds, 
                         _eta, _max_depth, num_features, alg, kernel_type)
    accuracies = zeros(nreps)
    features = 0
    for i = 1:nreps
        println("running experiment number: ", i)
        accuracies[i], features = get_accuracy(dset1, dset2, field, normalization, balance, sampling_type, feature_selection_type, p, num_features, 
                        alg, kernel_type, nrounds, _eta, _max_depth)
    end
    return mean(accuracies), Int(mean(features))
end


function run_combination(dset1, dset2, normalization_types, balance_types, sampling_type, feature_selection_type,
                         p, nreps, nrounds, _eta, _max_depth, num_features, alg, kernel_type,
                         ofolder)
    res = []
    ind = 0
    for j = 1:length(num_features)
        for nt in normalization_types
            for bt in balance_types
                accuracy, features_ran = run_ml(dset1, dset2, nt, bt, sampling_type, feature_selection_type, p, nreps, nrounds, 
                         _eta, _max_depth, num_features[j], alg, kernel_type)
                if ind == 0
                    res = [features_ran nt bt accuracy]
                    ind += 1
                else
                    res = [res; [features_ran nt bt accuracy]]
                end
            end
        end
    end
    fname = split(split(dset1,"/")[end],"_")[4] * "_" * split(split(dset2,"/")[end],"_")[4] * "_" * alg
    if alg == "svm"
        fname = fname * "_" * string(kernel_type)
    end
    header = "num_features,normalization,balanced,accuracy"
    DataRW.write_to_file(fname, ofolder, res, header)
end


function check_feature_selection(dset1, dset2, normalization_types, balance_types, 
                                        sampling_type, feature_selection_types, p, nreps, nrounds, 
                                        _eta, _max_depth, num_features, alg, kernel_type)
    
    res = []
    ind = 0
    for j = 1:length(num_features)
        for feature_selection_alg in feature_selection_types
            for nt in normalization_types
                for bt in balance_types        
                    accuracy, features_ran = run_ml(dset1, dset2, nt, bt, sampling_type, feature_selection_alg, p, nreps, nrounds, 
                            _eta, _max_depth, num_features[j], alg, kernel_type)
                    if ind == 0
                        res = [features_ran feature_selection_alg nt bt accuracy]
                        ind += 1
                    else
                        res = [res; [features_ran feature_selection_alg nt bt accuracy]]
                    end
                end
            end
        end
    end
    fname = split(split(dset1,"/")[end],"_")[4] * "_" * split(split(dset2,"/")[end],"_")[4] * "_" * alg
    if alg == "svm"
        fname = fname * "_" * string(kernel_type)
    end
    header = "num_features,sampling,normalization,balanced,accuracy"
    DataRW.write_to_file(fname, ofolder, res, header)
    
end


function run_full_dataset(dsets_list, field, normalization_type, balance, sampling_type, p, feature_selection_type,
                         nfeatures, alg, nrounds)
    @show normalization_type, balance, feature_selection_type, nfeatures, p, alg, nrounds
    # 1. read datasets
    dsets = Dict()
    println("reading data...")
    for dset in dsets_list
        dset_name = split(split(dset, "/")[end],"_")[4]
        dsets[dset_name] = read_data(dset, field)
    end
    
    dset_points = Int.(zeros(length(dsets_list)))
    ncols = 0
    for i = 1:length(dsets_list)
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        dset_points[i], ncols = size(dsets[dset_name])
    end

    X = []
    y = zeros(1)
    for i = 1:length(dsets_list)
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        if i == 1
            X = dsets[dset_name]
            y = zeros(size(X,1))
        else
            dset = dsets[dset_name]
            X = [X;dset]
            y = [y;(i-1)*ones(size(dset,1))][:]
        end
    end

     # 2. normalize
    if normalization_type == "z_scores"
        println("normalizing data...")
        X = DataRW._z_score_transform(X)
    end


    start_range = 1
    end_range = Int(dset_points[1])
    for i = 1:length(dsets_list)-1
        dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
        dsets[dset_name] = X[start_range:end_range,:]
        start_range = end_range + 1
        end_range = end_range + dset_points[i+1]
    end
    
    
    # 3. balance the data so that all classes have roughly equal number of members
    X_rejected = []
    y_rejected = []
    if balance
        n = minimum(dset_points) 
        X = []
        y = zeros(1)
        for i = 1:length(dsets_list)
            dset_name = split(split(dsets_list[i], "/")[end],"_")[4]
            X_sel, X_rej = _get_downsampled_dataset(dsets[dset_name], Int(n), sampling_type)
            y_sel = (i-1)*ones(size(X_sel,1))
            y_rej = (i-1)*ones(size(X_rej,1))
            if i == 1
                X = X_sel
                y = y_sel

                X_rejected = X_rej
                y_rejected = y_rej
            
            else
                X = [X; X_sel]
                y = [y; y_sel]

                X_rejected = [X_rejected; X_rej]
                y_rejected = [y_rejected; y_rej]
            end
        end
    end

    # 5. split into train and test sets
    println("splitting into training and testing sets...")
    train_X, test_X = DataRW.split_data([X y], p)

    train_y = train_X[1:end,end:end][:]     # training labels
    train_X = train_X[1:end,1:end-1]        # the training data points

    test_y = test_X[1:end, end:end][:]      # testing labels
    test_X = test_X[1:end, 1:end-1]         # the testing data points
     

    # 6. select features
    println("selecting features...")
    if nfeatures < size(train_X, 2) # need to sample features
        idx = _get_features_indices(feature_selection_type, train_X, train_y, nfeatures)
        train_X = train_X[:,idx]
        test_X = test_X[:,idx] 
        if balance
            X_rejected = X_rejected[:,idx]
        end
    end
    @show size(train_X), size(test_X), countmap(test_y)
    
    # 7. train the model
    println("beginning training, and testing the trained model")
    train_X, train_y = DataRW.shuffle_dataset(train_X, train_y)
    yp_rejected = []
    if alg == "svm"
        println("\nRunning SVM\n")
        model, t, w, b = svm.train_model(train_X, train_y)
        yp_train = svm.predict_labels(model, train_X) 
        yp_test = svm.predict_labels(model, test_X)
        if balance
            yp_rejected = svm.predict_labels(model,X_rejected)
        end
    elseif alg == "xgboost"    
        println("\nRunning XGBoost\n")
        bst = xgboost_lib._train_xgboost(train_X, train_y, nrounds, length(dsets_list))
        yp_train = xgboost_lib._make_predictions(train_X, bst) 
        yp_test = xgboost_lib._make_predictions(test_X, bst)
        if balance
            yp_rejected = xgboost_lib._make_predictions(X_rejected, bst)
        end
    end

    accuracy_train = PerformanceMetrics._accuracy(train_y, yp_train) 
    accuracy_test = PerformanceMetrics._accuracy(test_y, yp_test)
    
    f1_train = PerformanceMetrics._f1score(train_y, yp_train)  
    f1_test = PerformanceMetrics._f1score(test_y, yp_test)
    
    if balance
        accuracy_rejected = PerformanceMetrics._accuracy(y_rejected, yp_rejected)
        f1_rejected = PerformanceMetrics._f1score(y_rejected, yp_rejected)
    else
        accuracy_rejected = 0.0
        f1_rejected = 0.0
    end

    return [accuracy_train f1_train accuracy_test f1_test accuracy_rejected f1_rejected]
end


parent_folder = pwd()
data_folder = "/Data/"
datasets = Dict("afib" => "challenge_training2017_cases_afib_ecg_corr_metrics.mat",
                "noisy" => "challenge_training2017_cases_noisy_ecg_corr_metrics.mat",
                "normal" => "challenge_training2017_cases_normal_ecg_corr_metrics.mat",
                "other" => "challenge_training2017_cases_other_ecg_corr_metrics.mat")

field = "all_corr_metrics"

afib = parent_folder*data_folder*datasets["afib"]
normal = parent_folder*data_folder*datasets["normal"]
noisy = parent_folder*data_folder*datasets["noisy"]
other = parent_folder*data_folder*datasets["other"]

X_afib = read_data(afib, field)
X_normal = read_data(normal, field)
X_noisy = read_data(noisy, field)
X_other = read_data(other, field)


n = min(size(X_afib,1), size(X_normal, 1), size(X_noisy, 1), size(X_other, 1))
X_afib_balanced, _ = _get_downsampled_dataset(X_afib, n, "cluster")
X_normal_balanced, _ = _get_downsampled_dataset(X_normal, n, "cluster")
X_noisy_balanced, _ = _get_downsampled_dataset(X_noisy, n, "cluster")
X_other_balanced, _ = _get_downsampled_dataset(X_other, n, "cluster")



X_afib_normal = [X_afib;X_normal]
X_afib_normal_normalized = DataRW._z_score_transform(X_afib_normal)
features_selected = [22,29,5,2,20,24,3,10,11,21]

clusters = kmeans(Yinyang(), X_afib_normal_normalized, length(features_selected))
findall(x->x==4, clusters.assignments)

clusters.assignments[6]
FeatureSelection.cluster_features_per_class(X_afib_normal_normalized, features_selected)


cor(X_afib_normal_normalized[:,4], X_afib_normal_normalized[:,4])

FeatureSelection.cluster_features_per_class_via_correlation(X_afib_normal_normalized, features_selected)
# normalization_types = ["none", "z_scores"]
# balance_types = [false true]
# sampling_type = "random"
# feature_selection_types = ["largest_features", "OMP", "Pearson"]
# p = 90.0
# nreps = 3
# nrounds = 5
# _eta = 1.0
# _max_depth = 6
# num_features = [Vector(1:10); 48]
# algs = ["svm"]#["xgboost", "svm"]
# ofolder = "Results/Full_Dataset"
# dsets = [afib normal noisy other]

# nparams = 6
# for alg in algs
#     final_result = []
#     index = 1
#     for nfeatures in num_features
#         for sampling_type in feature_selection_types
#             for normalization in normalization_types
#                 for balance in balance_types            
#                     results = zeros(nreps, nparams)
#                     for i = 1:nreps
#                         println("rep #: ", i)
#                         results[i,:] = run_full_dataset(dsets, field, normalization, balance, 
#                                                             sampling_type, p, sampling_type, 
#                                                             nfeatures, alg, nrounds)
#                     end
#                     results = mean(results, dims=1)
#                     curr_settings = [nfeatures sampling_type normalization balance]
#                     curr_res = [curr_settings results]
#                     if index == 1
#                         final_result = curr_res
#                     else
#                         final_result = [final_result; curr_res]
#                     end
#                     index += 1
#                 end
#             end
#         end
#     end  
#     fname = alg
#     header = "nfeatures,sampling_technique,normalization_type,balanced,accuracy_train,f1_train,accuracy_test,f1_test,accuracy_rejected,f1_rejected"
#     DataRW.write_to_file(fname, ofolder, final_result, header)
# end

# X, y, Xr, yr = run_full_dataset(dsets, field, "z_scores", false, "random", 90.0, "largest_features", 1, "xgboost", 5)
