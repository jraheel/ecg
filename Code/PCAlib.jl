include("DataRW.jl")

module PCAlib
using MultivariateStats
using Plots

function getPCA(M, out_dim)
    return fit(PCA, M; maxoutdim=out_dim)
end

function getVariancePercentages(P, out_dim)
    principal_vars = principalvars(P)
    cummulative_principal_vars = zeros(out_dim)
    cummulative_principal_vars[1] = principal_vars[1]
    for i = 2:out_dim
        cummulative_principal_vars[i] = principal_vars[i] + cummulative_principal_vars[i-1]
    end
    return 100 .* cummulative_principal_vars./tvar(P), 100*tprincipalvar(P)/tvar(P)
end

function plot_cummulative_vars(cummul_prin_vars, threshold, out_dim)
    plt = plot(cummul_prin_vars)
    xlabel!("Principal Components")
    ylabel!("Cummulative Percentage Variance (%)")
    plot!(plt,threshold .* ones(out_dim))
    plot!(plt, size=(1300,1000))
end

end # end of PCAlib module


# data_folder = pwd() * "/Data/"
# M = DataRW.read_HGDP_mat_files(data_folder * "HGDP/HAPMAP_HGDP_WORLD_NO_OUTLIERS_1.mat", ["EUROPE"])

# X = (M[1:end, 1:end-1])
# y = M[1:end, end:end][:]

# odim = 2061
# P = PCAlib.getPCA(X, odim)

# cumm_vars, per_var = PCAlib.getVariancePercentages(P, odim)
# per_var
# PCAlib.plot_cummulative_vars(cumm_vars, 95.0, odim)


