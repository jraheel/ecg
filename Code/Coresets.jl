module Coresets

using LinearAlgebra
using ParallelKMeans
using Distances
using Statistics
using StatsBase

function sample_via_clustering(M, k::Int)
    """
        function sample_via_clustering(M, k::Int)
    Uses k-means clustering to compute k clusters of the points contained in M. Then selects one
    point per cluster to return a total of k points. Used to downsamples a dataset.

    Inputs: 2 inputs
        - M: the matrix of datapoints. M[i,:] is the i-th datapoint
        - k: the number of points required
    
    Outputs: 1 output
        - a vector of size k where each value in k is an index for the row of M
    """
    @assert k <= size(M,1)                  # can not sample more that the number of points in M
    clusters = kmeans(Yinyang(), M',k)      # perform k-means clustering. Notice it is on M', not M
    inds = Dict(i => 0 for i = 1:maximum(clusters.assignments)) # a dictionary to check points from which clusters have already been gathered
    seen_clusters = 0                       # a counter to keep tabs on the number of clusters already seen
    for i = 1:length(clusters.assignments)
        if inds[clusters.assignments[i]] == 0   # this particular cluster has not been encountered before
            inds[clusters.assignments[i]] = i   # mark this cluster as seen, and store the index of the point at clusters[<cluster number>]
            seen_clusters += 1              # one more cluster has been seen
        end
        if seen_clusters == k               # points from all clusters seen once
            break
        end
    end
    selected_inds = collect(values(inds))
    rejected_inds = collect(setdiff(Set(1:size(M,1)), Set(selected_inds))) 
    
    return M[selected_inds, :], M[rejected_inds, :]
end


function random_sample(M, k::Int)
    """
        function random_sample(M, k)

    samples k rows uniformly at random of M to return a matrix of size k-by-d given the original
    n-by-d matrix

    Inputs: 2 inputs
        - M: a matrix of Float64
        - k: the number of rows required
    
    Outputs: 1 output
        - a vector of size k where each value in k is an index for the row of M
    """
    selected_inds = sample(axes(M,1),k, replace=false)    # randomly sample indices from 1 to size(M,1)
    rejected_inds = collect(setdiff(Set(1:size(M,1)), Set(selected_inds))) 
    return M[selected_inds, :], M[rejected_inds, :]
end

end # end of Coreset module