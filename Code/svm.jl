module svm

using LIBSVM
using Statistics

function train_model(X_train, y_train, k=LIBSVM.Kernel.RadialBasis)
    """
        function train_model(X_train, y_train, k)
    runs SVM algorithm on the dataset provided using the specified Kernel for binary 
    classification. 

    Inputs: 3 inputs
        - X_train: the matrix containing the datapoints. X_train[i,:] is the i-th datapoint
        - y_train: the vector of labels. y_train[i] = +1 or -1 (binary classes only), is the label for X_train[i,:]
        - k      : the specified kernel from LIBSVM, e.g. LIBSVM.Kernel.Linear
    
    Outputs: 4 outputs
        - model: the trained model 
        - t    : the time it took for training in seconds
        - w    : the vector of parameters after training from the SVM objective function, or
                 the SVM decision function
        - b    : negative of the rho in SVM decision function sign(w' * x - rho)
    """
    t = @elapsed model = svmtrain(X_train', y_train, kernel = k)
    b = - model.rho
    w = model.SVs.X * model.coefs 

    if model.labels[1] == -1
        w = -w
        b = -b
    end
    return model, t, w, b
end

function compute_accuracy(model, X_test, y_test)
    """
        compute_accuracy(model, X_test, y_test)
    computes the vector of predictions and then the accuracy against the ground truth

    Inputs: 3 inputs
        - model : the SVM model from LIBSVM after training 
        - X_test: the matrix containing test datapoints. X_test[i,:] is the i-th test point
        - y_test: the vector of true labels. y_test[i] = +1 or -1, and is the label for X_test[i,:]
    
    Outputs: 1 output
        - the accuracy of prediction as a percentage of the total predictions
    """
    ŷ, _ = svmpredict(model, X_test')
    return mean(ŷ .== y_test) * 100
end


function predict_labels(model, X_test)
    """
        function predict_labels(model, X_test)
    
    generates predictions for datapoints using a trained SVM model. Wraps around svmpredict()
    function from LIBSVM
    
    Inputs: 2 inputs
        - model: a trained SVM model
        - X_test: matrix of datapoints. X[i,:] is the i-th datapoint
    
    Outputs: 1 output
        - the vector of labels predicted using the SVM model. the i-th entry in the vector
          is the predicted label for the i-th datapoint
    """
    ŷ, _ = svmpredict(model, X_test')
    return ŷ
end


function compute_accuracy_manually(X, y, w, b)
    """
        compute_accuracy_manually(X, y, w, b)
    uses the individual parameters of the SVM decision function, sign(w' * x + b) to compute
    predictions and then test for accuracy against the ground truth.

    Inputs: 4 inputs
        - X: the matrix containing test datapoints. X[i,:] is the i-th test point
        - y: the vector of true labels. y[i] = +1 or -1, and is the label for X[i,:]
        - w: the vector of parameters after training from the SVM objective function, or
             the SVM decision function
        - b: negative of the rho in SVM decision function sign(w' * x - rho)
    """
    ŷ = sign.(X * w .+ b[1])[:]
    return mean(ŷ .== y) * 100
end

end # end of module svm