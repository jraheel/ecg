module PerformanceMetrics
using MLJ
using ScikitLearn
@sk_import metrics: f1_score

function _accuracy(y, yp)
    """
        function accuracy(y, yp)
    
    computes the accuracy of predictions in the sense that for how many datapoints do the 
    predicted label matches the true label. Wraps around the accuracy function defined in 
    MLJ library

    Inputs: 2 inputs
        - y: the true labels. y[i] has the true label for the i-th datapoint
        - yp: the predicted labels. yp[i] has the predicted label for the i-th datapoint
    
    Outputs: 1 output
        - the percentage of datapoints for whom predicted label matched the true label
    """
    return MLJ.accuracy(y, yp)*100
end

function _confusion_matrix(yp, y)
    """
        function _confusion_matrix(yp, y)
    
    computes the confusion matrix for predictions given ground truth. Wraps around the 
    function defined in MLJ library

    Inputs: 2 inputs
        - y: the true labels. y[i] has the true label for the i-th datapoint
        - yp: the predicted labels. yp[i] has the predicted label for the i-th datapoint
    
    Outputs: 1 output
        - the confusion matrix for the predictions given ground truth
    """
    return MLJ.ConfusionMatrix()(y, yp)
end

function _f1score(y, yp, average=micro_avg)
    """
        function _f1score(y, yp, average)
    
    computes the fscore for β = 1.0 for the predictions given ground truth. Wraps around the
    f1_score function defined in ScikitLearn library
    
    Inputs: 3 inputs
        - y: the true labels. y[i] has the true label for the i-th datapoint
        - yp: the predicted labels. yp[i] has the predicted label for the i-th datapoint
        - average: kind of average to use when computing the final f-score for the entire dataset.
                   Allowed values are "weighted", "macro", and "micro". Please refer to documentation
                   of F-score for the explanation of the different kinds of averages.
    """
    # return f1_score(yp, y, average=average)*100
    return MulticlassFScore()(yp, y) * 100
end

end # end of module PerformanceMetrics