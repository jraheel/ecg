# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Anyone looking to apply SVM or XGBoost to ECG data

### How do I get set up? ###

The files such as DataRW.jl etc. have various functions to train SVMs or XGBoost
models and to make predictions or compute accuracy of prediction on test sets.
The main.jl should be ignored.

### Contribution guidelines ###

You could write tests or debug the functions

### Who do I talk to? ###

* Repo owner or admin
